FROM node:12-alpine AS compile-image

ADD . /src
WORKDIR /src
RUN apk add git
RUN npm install -g @angular/cli
RUN npm install
RUN ng build --prod

FROM nginx
COPY docker/nginx/default.conf /etc/nginx/conf.d/default.conf
COPY --from=compile-image /src/dist/dorkaar-lead-manager /usr/share/nginx/html
