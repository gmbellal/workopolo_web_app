dev-setup:
	docker	volume	create	lm_nodemodules
dev-install:
	docker-compose	-f	docker-compose.builder.yml	run	--rm	install
dev:
	docker-compose	up
build-image:
	docker	build	-t	dorkaar-lead-manager	.