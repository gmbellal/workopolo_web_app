export const environment = {
  production: true,
  firebase: {
    apiKey: "YOUR_APIKEY",
    authDomain: "YOUR_AUTH_DOMAIN",
    databaseURL: "YOUR_DATABASE_URL",
    projectId: "YOUR_PROJECT_ID",
    storageBucket: "universal-angular-85d7e.appspot.com",
    messagingSenderId: "YOUR_MESSAGE_SENDER_ID",
    appId: "YOUR_APPID"
  },
  server: {
    baseUrl: ''
  }
};
