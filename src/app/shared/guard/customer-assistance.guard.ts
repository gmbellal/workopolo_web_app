import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {AuthService} from '../services/firebase/auth.service';
import {Observable} from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class CustomerAssistanceGuard implements CanActivate {
    constructor(public authService: AuthService,
                public router: Router) { }
    canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
        // Guard for user is login or not
        const user = JSON.parse(localStorage.getItem('userSession'));
        if (!user) {
            this.router.navigate(['/auth/login']);
            return true;
        }
        return true;
    }
}
