import { Routes } from '@angular/router';
import { AdminGuard } from '../guard/admin.guard';
import { CustomerAssistanceGuard } from '../guard/customer-assistance.guard';

export const content: Routes = [
  {
    path: 'home',
    loadChildren: () => import('../../pages/home/home.module').then(m => m.HomeModule),
    canActivate: [CustomerAssistanceGuard],
    data: {
      breadcrumb: 'Home'
    }
  }
  


];
