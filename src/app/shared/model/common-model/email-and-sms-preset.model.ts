export class EmailAndSmsPresetModel {
    _id: string;
    createdAt: Date;
    name: string;
    from: string;
    subject: string;
    body: string;
    type: string;
}
