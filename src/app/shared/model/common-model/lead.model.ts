export class LeadModel {
    constructor() {
        this.leadNotes = [];
        this.newNotes = [];
        this.responding = false;
    }
    _id: string;
    submitedAt: any;
    sellerName: string;
    contactNo: string;
    shopName: string;
    shopAddress: string;
    shopCategory: string;
    email: string;
    status: string;
    leadNotes: any[];
    newNotes: any[];
    active: boolean;
    createdAt: Date;
    responding: boolean;

}
