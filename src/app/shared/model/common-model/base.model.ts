export class BaseModel {
    _id: string;
    createdAt: Date;
    updatedAt: Date;
    _v: number;
}
