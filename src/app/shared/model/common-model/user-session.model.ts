export class UserSessionModel {
    constructor(token, userData) {
        this.token = token;
        this.userData = userData;
    }

    token: string;
    userData: [];
    role: string = "admin";
}
