import {BaseModel} from './base.model';

export class SettingModel extends BaseModel {
    constructor() {
        super();
        this.presetId = '';
    }
    name: string;
    type: string;
    description: string;
    value: any;
    presetId: string;
}
