export class user {
    id: string;
    name: string ;
}
export class category {
    id: string;
    titleEn: string;
    titleBn: string
    icon: string;
}
export class demand {
    title: string;
    details: [];
}
export class isDeleted {
    flag: boolean;
    reason: string;
}
export class location {
    type:string;
    coordinates: [number];
    pseudoCords: [Number];
    shareRealLocation: boolean;
}



export class DorkaarDemandModel {
    constructor() {
        this.userNotes = [];
        this.newNotes = [];
    }
    user = new user();
    category= new category();
    location= new location();
    demand= new demand();
    isDeleted=new isDeleted();
    offers: [];
    deadline: string;
    isApproved: boolean;
    isFulfilled: boolean;
    //notes
    userNotes: any[];
    newNotes: any[];
}
