export class DorkaarUserModel {
    constructor() {
        this.userNotes = [];
        this.newNotes = [];
    }
    _id: string;
    name: any;
    phone: string;
    isValid: boolean;
    createdAt: Date;
    updatedAt: Date;
    userNotes: any[];
    newNotes: any[];
}
