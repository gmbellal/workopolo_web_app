export class user {
    id: string;
    name: string ;
}
export class time {
    hour: number;
    min: number;
}
export class range {
    from = new time();
    to = new time();
}
export class openingHours {
    sat = new range();
    sun = new range();
    mon = new range();
    tue = new range();
    wed = new range();
    thu = new range();
    fri = new range();
}
export class category {
    id: String;
    titleEn: string;
    titleBn: string;
    icon: string
}

export class DorkaarShopModel {
    constructor() {
        this.userNotes = [];
        this.newNotes = [];
    }
    _id: string;
    shopName: string;
    category = new category();
    contactNumber: String;
    messageStatus: String;
    homeDelivery: Boolean;
    deliveryRadius: Number;
    deliveryCharge: Number;
    productList: String;
    openingHours = new openingHours;
    isDeleted: Boolean;
    isVerified: Boolean;
    isOpenNow: Boolean;
    user = new user();
    storelocation: {
        type: { type: String },
        coordinates: [Number]
    };
    viewCount: number;
    
    userNotes: any[];
    newNotes: any[];
    status: string;
}
