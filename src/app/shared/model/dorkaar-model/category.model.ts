export class categoryInfo {
    titleEn: string;
    titleBn: string ;
}


export class DorkaarCategoryModel {
    constructor() {
        this.userNotes = [];
        this.newNotes = [];
    }

    _id: string;
    categoryInfo= new categoryInfo();
    icon: string;
    catType: string;
    
    userNotes: any[];
    newNotes: any[];
    status: string;
}



