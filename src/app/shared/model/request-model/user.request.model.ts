export class UserRequestModel {
    constructor() {
        this.gender = 'male';
        this.role = new Role();
    }
    email: string;
    firstName: string;
    lastName: string;
    password: string;
    confirmPassword: string;
    roleId: any;
    role: Role;
    gender: string;
    picture: string;

}

class Role {
    _id: string;
    name: string;
}


export class UserViewModel {
    _id: string;
    email: string;
    firstName: string;
    lastName: string;
    password: string;
    confirmPassword: string;
    roleId: any;
    role: Role;
    gender: string;
    picture: string;
    createdAt: Date;

}
