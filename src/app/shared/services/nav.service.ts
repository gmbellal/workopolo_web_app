import {Injectable, HostListener, OnInit} from '@angular/core';
import { BehaviorSubject, Observable, Subscriber } from 'rxjs';
import {AuthService} from './firebase/auth.service';

// Menu
export interface Menu {
	path?: string;
	title?: string;
	icon?: string;
	type?: string;
	badgeType?: string;
	badgeValue?: string;
	active?: boolean;
	bookmark?: boolean;
	children?: Menu[];
}

@Injectable({
	providedIn: 'root'
})

export class NavService{

	public screenWidth: any;
	public collapseSidebar: boolean = false;
	public viewMenus: Menu[] = [];
	adminPermissions = ['Dashboard', 'Home','Settings'];
	nonAdminPermissions = ['Dashboard',  'Home', 'Settings'];
	constructor(private authService: AuthService) {
		this.onResize();
		if (this.screenWidth < 991) {
			this.collapseSidebar = true;
		}
		this.genMenu();
	}


	genMenu() {
		if(this.authService.userSession !== undefined){
			if (this.authService.userSession.role === 'Admin') {
				this.MENUITEMS.forEach((menu) => {
					this.adminPermissions.forEach(permission=>{
						if(permission === menu.title) {
							this.viewMenus.push(menu);
						}
					})
				});
			} else {
				this.MENUITEMS.forEach((menu) => {
					this.nonAdminPermissions.forEach(permission=>{
						if(permission === menu.title) {
							this.viewMenus.push(menu);
						}
					})
				});
			}
		}
	}

	// Windows width
	@HostListener('window:resize', ['$event'])
	onResize(event?) {
		this.screenWidth = window.innerWidth;
	}

	MENUITEMS: Menu[] = [
		{
			title: 'Home', icon: 'home', type: 'sub', badgeType: 'primary', active: true, children: [
				{ path: '/home/workopolo', title: 'Workopolo', type: 'link' }
			]
		},
		{
			title: 'Settings', icon: 'settings', type: 'sub', active: false, children: [
				{ path: '/settings/email-setting', title: 'Email Setting', type: 'link' },
				{ path: '/settings/sms-setting', title: 'SMS Setting', type: 'link' },
			]
		}
		
	]
	// Array
	items = new BehaviorSubject<Menu[]>(this.viewMenus);


}
