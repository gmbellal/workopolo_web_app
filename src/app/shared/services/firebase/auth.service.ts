import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import {UserSessionModel} from '../../model/common-model/user-session.model';

export interface User {
  uid: string;
  email: string;
  displayName: string;
  photoURL: string;
  emailVerified: boolean;
}
class UserModel {
  email: string;
  password: string;
  token: string;
  firstName: string;
  lastName: string;
  role: string;
  gender: string;

  facebook: string;
  tokens: any;

  profile: {
    name: string;
    gender: string;
    location: string;
    website: string;
    picture: string;
  };


}

@Injectable({
  providedIn: 'root'
})
export class AuthService  {
  public userSession: UserSessionModel;

  constructor(
    public router: Router,
    public toster: ToastrService,
              ) {
    const userSessionString = localStorage.getItem('userSession');
    if (userSessionString != null) {
      const userSession = JSON.parse(userSessionString);
      this.userSession = userSession;
    }
  }
  // Sign out
  SignOut() {
      localStorage.clear();
      this.userSession = null;
      this.router.navigate(['/auth/login']);
  }

  get isLoggedIn(): boolean {
    const user = JSON.parse(localStorage.getItem('user'));
    return (user != null && user.emailVerified != false) ? true : false;
  }


}
