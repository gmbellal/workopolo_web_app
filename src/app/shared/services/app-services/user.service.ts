import {Injectable} from '@angular/core';
import {HttpService} from './http.service';
import {environment} from '../../../../environments/environment';
import {UserRequestModel} from '../../model/request-model/user.request.model';
import { analytics } from 'firebase';

@Injectable({
    providedIn: 'root'
})
export class UserService {
    constructor(private http: HttpService) {
    }
    
    checkCredentials(loginModel: any) {
        return this.http.post(environment.server.baseUrl + '/api/v4/en/login', loginModel);
    }


    
}
