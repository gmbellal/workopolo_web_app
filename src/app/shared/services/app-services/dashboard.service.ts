import {Injectable} from '@angular/core';
import {HttpService} from './http.service';
import {environment} from '../../../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class DashboardService {
    constructor(private http: HttpService) {
    }
    
    getUserWidgets(data: any) {
        return this.http.get(environment.server.baseUrl+'/api/v4/en/dashboard/get_dashboard?user_id='+data.user_id+"&employee_id="+data.user_id+"&session_key="+data.session_key);
    }

    getUserWidgetSummary(data: any) {
        return this.http.get(environment.server.baseUrl+'/api/v4/en/dashboard/get_dashboard_summary?user_id='+data.user_id+"&employee_id="+data.user_id+"&session_key="+data.session_key+"&filter_type="+data.filter_type+"&item_key="+data.item_key);
    }





}
