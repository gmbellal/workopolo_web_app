import {Injectable} from '@angular/core';
import {HttpService} from './http.service';
import {environment} from '../../../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class SetupDataService {
    constructor(private http: HttpService) {    }
    getRoles() {
        return this.http.get(environment.server.baseUrl + 'api/role');
    }
    getPresetById(id) {
        return this.http.get(environment.server.baseUrl + 'api/preset/' + id);
    }
    getSMSPresetSetup() {
        return this.http.get(environment.server.baseUrl + 'api/preset/set-up/sms');
    }
    getEmailPresetSetup() {
        return this.http.get(environment.server.baseUrl + 'api/preset/set-up/email');
    }
    getEmailPreset() {
        return this.http.get(environment.server.baseUrl + 'api/preset/email');
    }
    savePreset(data) {
        return this.http.post(environment.server.baseUrl + 'api/preset', data);
    }

    updatePreset(data) {
        return this.http.put(environment.server.baseUrl + 'api/preset', data);
    }

    getPresetEmailDtOptions(callBackFunc): DataTables.Settings {
        const url = 'api/preset/email/data-table';
        const options: DataTables.Settings = {
            autoWidth: false,
            pagingType: 'full_numbers',
            pageLength: 10,
            serverSide: true,
            processing: true,
            order:[[ 1, 'asc' ]],
            ajax: (dataTablesParameters: any, callback) => {
                this.http
                    .post(
                        environment.server.baseUrl + url,
                        dataTablesParameters
                    ).subscribe(resp => {
                    callBackFunc(resp);
                    callback({
                        recordsTotal: resp.data.recordsTotal,
                        recordsFiltered: resp.data.recordsFiltered,
                        data: []
                    });
                });
            },
            columns: [{orderable: false} , { data: 'name' }, { data: 'from'}, { data: 'subject' },
                { orderable: false}, { data: 'createdAt'}]
        };
        return options;
    }

    getPresetSmsDtOptions(callBackFunc): DataTables.Settings {
        const url = 'api/preset/sms/data-table';
        const options: DataTables.Settings = {
            autoWidth: false,
            pagingType: 'full_numbers',
            pageLength: 10,
            serverSide: true,
            processing: true,
            order:[[ 1, 'asc' ]],
            ajax: (dataTablesParameters: any, callback) => {
                this.http
                    .post(
                        environment.server.baseUrl + url,
                        dataTablesParameters
                    ).subscribe(resp => {
                    callBackFunc(resp);
                    callback({
                        recordsTotal: resp.data.recordsTotal,
                        recordsFiltered: resp.data.recordsFiltered,
                        data: []
                    });
                });
            },
            columns: [{orderable: false} , { data: 'name' }, { data: 'from'}, { data: 'subject' },
                { orderable: false}, { data: 'createdAt'}]
        };
        return options;
    }

}
