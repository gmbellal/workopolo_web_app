import {Injectable} from '@angular/core';
import {environment} from '../../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {HttpService} from './http.service';
import {LeadModel} from '../../model/common-model/lead.model';

@Injectable({
    providedIn: 'root'
})
export class LeadsService {
    constructor(private httpClient: HttpClient, private http: HttpService) {
    }

    getLeadDtOptions(callBackFunc): DataTables.Settings {
        const url = 'api/lead/data-table';
        const options: DataTables.Settings = {
            autoWidth: false,
            pagingType: 'full_numbers',
            pageLength: 10,
            serverSide: true,
            processing: true,
            order:[[ 7, 'asc' ]],
            ajax: (dataTablesParameters: any, callback) => {
                this.http
                    .post(
                        environment.server.baseUrl + url,
                        dataTablesParameters
                    ).subscribe(resp => {
                    callBackFunc(resp);
                    callback({
                        recordsTotal: resp.data.recordsTotal,
                        recordsFiltered: resp.data.recordsFiltered,
                        data: []
                    });
                });
            },
            columns: [{orderable: false} , { data: 'sellerName' }, { orderable: false}, { data: 'shopName' },
                { orderable: false}, { orderable: false},{orderable: false}, { data: 'submitedAt' }, { data: 'status' }]
        };
        return options;
    }

    getLead(id: string) {
        return this.http.get(environment.server.baseUrl + 'api/lead/' + id);
    }

    getLeadResponder() {
        return this.http.get(environment.server.baseUrl + 'api/leads/responder' );
    }

    updateStatus(lead: LeadModel) {
         return this.http.put(environment.server.baseUrl + 'api/lead/update', lead);
    }

    nextLeadResponder(lead: LeadModel) {
         return this.http.put(environment.server.baseUrl + 'api/leads/responder-next', lead);
    }
}
