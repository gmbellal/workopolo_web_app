import {Injectable, OnInit} from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {NgxSpinnerService} from 'ngx-spinner';
import {ToastrService} from 'ngx-toastr';
import {Observable, throwError} from 'rxjs';
import {AuthService} from '../firebase/auth.service';
import {catchError, map} from 'rxjs/operators';
import {ResponseModel} from '../../model/common-model/response.model';
@Injectable({
    providedIn: 'root'
})
export class HttpService {
    private busy = 0;

    httpOptions: any;
    constructor(
        private http: HttpClient,
        private spinner: NgxSpinnerService,
        private toastr: ToastrService,
        private authService: AuthService
    ) { }

    genHeaderOption() {
        if (this.authService.userSession) {
            this.httpOptions = {
                headers: new HttpHeaders({
                    'Content-Type':  'application/json',
                    'Authorization': '',
                    'Access-Control-Allow-Origin': '*'
                })
            };
        }
    }

    get(url) {
        this.checkToBusyShow();
        this.busy++;
        this.genHeaderOption();
        return this.http.get(url, this.httpOptions).pipe(
            map((data: any) => {
                this.busy--;
                this.checkToBusyHide();
                if (data.status === 500) {
                    this.toastr.error(data.message, 'Failed');
                    console.log(data.error);
                    return null;
                }
                return data;
            }),
            catchError((err) => {
                this.busy--;
                this.checkToBusyHide();
                this.toastr.error('Please Try Again', 'Failed');
                console.log(err);
                return err;
            })
        );
    }

    post(url, data) {
        this.checkToBusyShow();
        this.busy++;
        this.genHeaderOption();
        return this.http.post(url, data, this.httpOptions).pipe(
            map(( data: any) => {
                this.busy--;
                this.checkToBusyHide();
                if (data.status === 500) {
                    this.toastr.error(data.message, 'Failed');
                    console.log(data.error);
                    return null;
                }
                return data;
            }),
            catchError((err) => {
                this.busy--;
                this.checkToBusyHide();
                this.toastr.error('Please Try Again', 'Failed');
                console.log(err);
                return err;
            })
        );
    }

    put(url, data) {
        this.checkToBusyShow();
        this.busy++;
        this.genHeaderOption();
        return this.http.put(url, data, this.httpOptions).pipe(
            map((data: any) => {
                this.busy--;
                this.checkToBusyHide();
                if (data.status === 500) {
                    this.toastr.error(data.message, 'Failed');
                    console.log(data.error);
                    return null;
                }
                return data;
            }),
            catchError((err) => {
                this.busy--;
                this.checkToBusyHide();
                this.toastr.error('Please Try Again', 'Failed');
                console.log(err);
                return err;
            })
        );
    }

    delete(url) {
        this.checkToBusyShow();
        this.busy++;
        this.genHeaderOption();
        return this.http.delete(url, this.httpOptions).pipe(
            map((data: any) => {
                this.busy--;
                this.checkToBusyHide();
                if (data.status === 500) {
                    this.toastr.error(data.message, 'Failed');
                    console.log(data.error);
                    return null;
                }
                return data;
            }),
            catchError((err) => {
                this.busy--;
                this.checkToBusyHide();
                this.toastr.error('Please Try Again', 'Failed');
                console.log(err);
                return err;
            })
        );
    }


    checkToBusyHide() {
        if (this.busy <= 0) {
            setTimeout(() => {
                this.spinner.hide();
            }, 300);
        }
    }

    checkToBusyShow() {
        if (this.busy <= 0) {
            this.spinner.show();
        }
    }
}
