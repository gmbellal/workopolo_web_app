import {Injectable} from '@angular/core';
import {HttpService} from '../http.service';
import {environment} from '../../../../../environments/environment';



@Injectable({
    providedIn: 'root'
})
export class CategoryService {
    constructor(private http: HttpService) {

    }


    getCategoryDtOptions(callBackFunc): DataTables.Settings {
        const url = 'api/dorkaar-category/data-table';
        const options: DataTables.Settings = {
            autoWidth: false,
            pagingType: 'full_numbers',
            pageLength: 10,
            serverSide: true,
            processing: true,
            order:[[ 1, 'asc' ]],
            ajax: (dataTablesParameters: any, callback) => {
                this.http
                    .post(
                        environment.server.baseUrl + url,
                        dataTablesParameters
                    ).subscribe(resp => {
                    callBackFunc(resp);
                    callback({
                        recordsTotal: resp.data.recordsTotal,
                        recordsFiltered: resp.data.recordsFiltered,
                        data: []
                    });
                });
            },
            columns: [{orderable: true} ,  { data: '_id'}]
        };
        return options;
    }

  
    getCategory(catId: string) {
        return this.http.get(environment.server.baseUrl + 'api/dorkaar-category/'+catId);
    }

    updateCategoryInfo(category) {
        return this.http.put(environment.server.baseUrl + 'api/dorkaar-category/update' ,category);
    }

    saveCategory(category) {
        return this.http.put(environment.server.baseUrl + 'api/dorkaar-category/save' ,category);
    }


}
