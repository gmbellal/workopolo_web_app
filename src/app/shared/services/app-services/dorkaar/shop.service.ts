import { Injectable } from '@angular/core';
import { HttpService } from '../http.service';
import { environment } from '../../../../../environments/environment';



@Injectable({
    providedIn: 'root'
})
export class ShopService {
    constructor(private http: HttpService) {

    }


    getShopDtOptions(callBackFunc): DataTables.Settings {
        const url = 'api/dorkaar-shop/data-table';
        const options: DataTables.Settings = {
            autoWidth: false,
            pagingType: 'full_numbers',
            pageLength: 10,
            serverSide: true,
            processing: true,
            order: [[1, 'desc']],
            ajax: (dataTablesParameters: any, callback) => {
                this.http
                    .post(
                        environment.server.baseUrl + url,
                        dataTablesParameters
                    ).subscribe(resp => {
                        callBackFunc(resp);
                        callback({
                            recordsTotal: resp.data.recordsTotal,
                            recordsFiltered: resp.data.recordsFiltered,
                            data: []
                        });
                    });
            },
            columns: [{ orderable: false },  { data: 'createdAt' } ]
        };
        return options;
    }

    getShop(shopId: string) {
        return this.http.get(environment.server.baseUrl + 'api/dorkaar-shop/' + shopId);
    }

    updateShopInfo(shop) {
        return this.http.put(environment.server.baseUrl + 'api/shop/update', shop);
    }

}
