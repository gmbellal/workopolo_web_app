import {Injectable} from '@angular/core';
import {HttpService} from '../http.service';
import {environment} from '../../../../../environments/environment';



@Injectable({
    providedIn: 'root'
})
export class UserService {
    constructor(private http: HttpService) {

    }


    getUserDtOptions(callBackFunc): DataTables.Settings {
        const url = 'api/dorkaar-user/data-table';
        const options: DataTables.Settings = {
            autoWidth: false,
            pagingType: 'full_numbers',
            pageLength: 10,
            serverSide: true,
            processing: true,
            order:[[ 2, 'desc' ]],
            ajax: (dataTablesParameters: any, callback) => {
                this.http
                    .post(
                        environment.server.baseUrl + url,
                        dataTablesParameters
                    ).subscribe(resp => {
                    callBackFunc(resp);
                    callback({
                        recordsTotal: resp.data.recordsTotal,
                        recordsFiltered: resp.data.recordsFiltered,
                        data: []
                    });
                });
            },
            columns: [{orderable: false} , { data: 'createdAt'}, { data: 'phone'}, {data: "isValid"}]
        };
        return options;
    }

  
    getUser(userId: string) {
        return this.http.get(environment.server.baseUrl + 'api/dorkaar-user/'+userId);
    }

    updateUserInfo(user) {
        return this.http.put(environment.server.baseUrl + 'api/dorkaar-user/update' ,user);
    }

    getTotalUser() {
        return this.http.get(environment.server.baseUrl + 'api/dorkaar-user/count');
    }

}
