import {Injectable} from '@angular/core';
import {HttpService} from '../http.service';
import {environment} from '../../../../../environments/environment';



@Injectable({
    providedIn: 'root'
})
export class demandService {
    constructor(private http: HttpService) {
    }




    getDemandDtOptions(callBackFunc): DataTables.Settings {
        const url = 'api/dorkaar-demand/data-table';
        const options: DataTables.Settings = {
            autoWidth: false,
            pagingType: 'full_numbers',
            pageLength: 10,
            serverSide: true,
            processing: true,
            order:[[ 5, 'desc' ]],
            ajax: (dataTablesParameters: any, callback) => {
                this.http
                    .post(
                        environment.server.baseUrl + url,
                        dataTablesParameters
                    ).subscribe(resp => {
                    callBackFunc(resp);
                    callback({
                        recordsTotal: resp.data.recordsTotal,
                        recordsFiltered: resp.data.recordsFiltered,
                        data: []
                    });
                });
            },
            columns: [{orderable: false} , { data: 'user.name' }, { data: 'status' }, { data: 'category.titleEn'}, { data: 'deadline' }, { data: 'createdAt'}, { data: 'updatedAt'}]
        };
        return options;
    }

    getDemandById( demandId) {
        return this.http.get(environment.server.baseUrl + 'api/dorkaar-demand/' + demandId);
    }


    updateDemandInfo(demand) {
        return this.http.put(environment.server.baseUrl + 'api/demand/update', demand);
    }


}
