import {Injectable} from '@angular/core';
import {HttpService} from '../http.service';
import {environment} from '../../../../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class MapService {
    constructor(private http: HttpService) {
    }

    getCoordinates(types: string, lat: number, lon: number, max_dis: number) {
        switch ( types ) {
            case 'shop':
                return this.http.get(environment.server.baseUrl + 'api/shop/coordinates/' + lat + '/' + lon + '/' + max_dis);
                break;
            case 'customer':
                return this.http.get(environment.server.baseUrl + 'api/dorkaar-user/coordinates/' + lat + '/' + lon + '/' + max_dis);
                break;
            case 'demand':
                return this.http.get(environment.server.baseUrl + 'api/demand/coordinates/' + lat + '/' + lon + '/' + max_dis);
                break;
            case 'delivery-person':
                return this.http.get(environment.server.baseUrl + 'api/delivery/coordinates/' + lat + '/' + lon + '/' + max_dis);
                break;
            default:
                return this.http.get(environment.server.baseUrl + 'api/shop/coordinates/' + lat + '/' + lon + '/' + max_dis);
        }
    }
}
