import {Injectable} from '@angular/core';
import {HttpService} from './http.service';
import {environment} from '../../../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class SettingsService {
    constructor(private http: HttpService) {
    }

    saveEmailEvent(data) {
        return this.http.post(environment.server.baseUrl + 'api/setting', data);
    }

    updateEmailEvent(data) {
        return this.http.put(environment.server.baseUrl + 'api/setting', data);
    }

    getAppSettingDtOptions(type, callBackFunc): DataTables.Settings {
        const url = 'api/setting/data-table/' + type;
        const options: DataTables.Settings = {
            autoWidth: false,
            pagingType: 'full_numbers',
            pageLength: 10,
            serverSide: true,
            processing: true,
            order:[[ 1, 'asc' ]],
            ajax: (dataTablesParameters: any, callback) => {
                this.http
                    .post(
                        environment.server.baseUrl + url,
                        dataTablesParameters
                    ).subscribe(resp => {
                    callBackFunc(resp);
                    callback({
                        recordsTotal: resp.data.recordsTotal,
                        recordsFiltered: resp.data.recordsFiltered,
                        data: []
                    });
                });
            },
            columns: [{orderable: false} , { data: 'name' }]
        };
        return options;
    }
}
