import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
// import { FormBuilder, Validators, FormGroup } from '@angular/forms';
// import { AngularFireAuth } from '@angular/fire/auth';
// import {ResponseModel} from '../../shared/model/common-model/response.model';
import { AuthService } from '../../shared/services/firebase/auth.service';
import {UserSessionModel} from '../../shared/model/common-model/user-session.model';
import {ToastrService} from 'ngx-toastr';
import {UserService} from '../../shared/services/app-services/user.service';
import {NavService} from '../../shared/services/nav.service';

class LoginModel {
  device_type: number = 3;
  device_key: string = "6awydaf$D"
  username: string = "mazba.cse@gmail.com";
  password: string = "wwwwww";
  device_id: string =  "AbF7238822j";
}

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})

export class LoginComponent implements OnInit {
  user: LoginModel;
  constructor(
      public userService: UserService,
      public router: Router,
      private toastr: ToastrService,
      private authService: AuthService,
      private navService: NavService) {
      this.user = new LoginModel();
  }

  ngOnInit() { }

  submit (form: any) {
    this.userService.checkCredentials(this.user).subscribe((res: any) => {
      if(res.error_code === 0){
        this.toastr.success(res.message, 'Success');
        this.login(res.data);
        console.log(res);
      }else{
        this.toastr.warning(res.message, 'Failed');
      }
    });
  }
  

  login(data: any) {
    const newUserSession = new UserSessionModel( data.session_key, data);
    const newUserSessionString = JSON.stringify(newUserSession);
    localStorage.setItem('userSession', newUserSessionString);
    this.authService.userSession = newUserSession;
    this.navService.genMenu();
    this.toastr.success('Successfully Loged In', 'Success');
    this.router.navigate(['/home/workopolo']);
  }



  
}
