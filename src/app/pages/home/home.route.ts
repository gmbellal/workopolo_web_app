import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';

import {ActivityComponent} from './activity/activity.component';

const routes: Routes = [
    {
        path: '',
        children: [
            {
                path: 'workopolo',
                component: ActivityComponent,
                data: {
                    title: 'Workopolo',
                    breadcrumb: 'workopolo'
                }
            }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class HomeRoutingModule { }
