import {NgModule} from '@angular/core';
import {SharedModule} from '../../shared/shared.module';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';
import {HomeRoutingModule} from './home.route';
import {DataTablesModule} from 'angular-datatables';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {AgmCoreModule} from '@agm/core';
import {AgmJsMarkerClustererModule} from '@agm/js-marker-clusterer';
import {ActivityComponent} from './activity/activity.component';
import {CountToModule} from 'angular-count-to';

@NgModule({
    declarations: [
        ActivityComponent
    ],
    imports: [
        SharedModule,
        HomeRoutingModule,
        NgxDatatableModule,
        DataTablesModule,
        CommonModule,
        FormsModule,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyD8EmQqCZlJRRr9GNnyFIfq_laKxUdQS1E'
        }),
        AgmJsMarkerClustererModule,
        CountToModule
    ]
})
export class HomeModule { }
