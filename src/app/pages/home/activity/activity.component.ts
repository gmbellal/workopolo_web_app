import {Component, OnInit} from '@angular/core';
import {UserService} from '../../../shared/services/app-services/dorkaar/user.service';
import {DashboardService} from '../../../shared/services/app-services/dashboard.service';
import {AuthService} from '../../../shared/services/firebase/auth.service';

@Component({
    selector: 'app-dashboard-app-stat',
    templateUrl: 'activity.component.html',
    styleUrls: ['./activity.component.scss']
})
export class ActivityComponent implements OnInit {
    userWidgets: any;
    activitySummery: any;
    itemView: number = 0;
    
    constructor(private dashboardService: DashboardService, public authService: AuthService) {
    }
    ngOnInit(): void {
        this.getUserWidgets();
    }


    getUserWidgets() {
        this.itemView = 1;
        //console.log(this.authService.userSession);
        const data: any = {
            "user_id" : this.authService.userSession.userData["user_id"],
            "employee_id" : this.authService.userSession.userData["user_id"],
            "session_key" : this.authService.userSession.userData["session_key"]
        }
        this.dashboardService.getUserWidgets(data).subscribe((res) => {
            this.userWidgets = res.data;
            //console.log(res.data);
        });
    }


    viewControl(view: number){
        this.itemView = view;
    }



    getUserWidgetSummary(itemKey: any) {
        const data: any = {
            "user_id" : this.authService.userSession.userData["user_id"],
            "employee_id" : this.authService.userSession.userData["user_id"],
            "session_key" : this.authService.userSession.userData["session_key"],
            "filter_type" : 1,
            "item_key"    : itemKey
        }
        this.dashboardService.getUserWidgetSummary(data).subscribe((res) => {
            this.activitySummery = res.data;
            this.itemView = 2;
            console.log(res.data);
        });
    }

    




    getUserActivities() {
        const data: any = {
            "user_id" : this.authService.userSession.userData["user_id"],
            "employee_id" : this.authService.userSession.userData["user_id"],
            "session_key" : this.authService.userSession.userData["session_key"]
        }
        this.dashboardService.getUserWidgets(data).subscribe((res) => {
            this.userWidgets = res.data;
            console.log(res.data);
        });
    }


}
